const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name, session_id');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

//TODO: in future this will be customizable
const port = 8000;

app.listen(port, function () {
    console.log(`we are live on ${port}`);
    require('./app/routes')(app);
});

app.get('/', function (req, res) {
    res.render('index.html', {});
});
