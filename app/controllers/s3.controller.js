const env = require('../config/s3.env.js');
const exif = require('exif-parser');
const s3 = require('../config/s3.config.js');
const {thumb} = require('node-thumbnail');

class S3Controller {

    static getCredentials(req, res) {
        const assetsPath = `${process.cwd()}/assets`;
        const s3Client = s3.s3Client;
        const params = s3.downloadParams;
        const path = `${assetsPath}/images/${req.params.filename}`;
        const fileStream = require('fs').createWriteStream(path).on('finish', () => {
            thumb({
                source: path, // could be a filename: dest/path/image.jpg
                destination: `${assetsPath}/thumb`,
                concurrency: 4
            }).then((files, err) => console.log('All done!'));
        });

        params.Key = req.params.filename;

        let isGetExif = false;

        s3Client.getObject(params)
            .createReadStream()
            .on('data', (chunk) => {
                if (!isGetExif) {
                    const result = exif.create(chunk).parse();
                    if (result && result.tags) {
                        isGetExif = true;
                        res.status(200).json(result.tags);
                    }
                }
            })
            .on('error', (err) => {
                res.status(500).json({error: "Error: " + err});
            })
            .pipe(fileStream);
    }

    static validateImageType(req, res, next) {
        if (req.body.type && (req.body.type === 'image/jpeg' || req.body.type === 'image/png')) {
            next();
        } else {
            res.status(422).json({message: 'Invalid file type, only JPEG and PNG is allowed!'});
        }
    }

    static getUploadUrl(req, res, next) {
        // Params that we use to create our signature
        const params = {
            Bucket: env.Bucket,
            Fields: {
                key: req.body.name,
                "acl": "public-read",
                "Content-Type": req.body.type,
            },
            Expires: 3600,
            Conditions: [
                // This depicts the ACL the file will have when uploaded
                {"acl": "public-read"},
                ["starts-with", "$Content-Type", "image/"]
            ],
        };

        // Use the aws-sdk method to create the signature
        const result = s3.s3Client.createPresignedPost(params);
        res.json(result);
    }
}

module.exports = S3Controller;