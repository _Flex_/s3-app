const s3Routes = require('./s3.router');

module.exports = function (app) {
    s3Routes(app);
};