const S3Controller = require('../controllers/s3.controller');

module.exports = function (app) {
    app.get('/files/:filename', S3Controller.getCredentials);
    app.post('/getUploadUrl', S3Controller.validateImageType, S3Controller.getUploadUrl);
};